<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use Request, Response;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;

class CredlyApi extends Model {

	/**
	 * 
	 * Username to authenticate with
	 * @var string
	 */
	protected $username;

	/**
	 * User password to authenticate with
	 * @var string
	 */
	protected $password;

	/**
	 * Base url for credly api
	 * @var string
	 */
	public $apiUrl;

	/**
     * Api key from credly api
     * 
     * @var string
     * @access protected
     */
	protected $apiKey;

	/**
	 * Secret key from credly api
	 * 
	 * @var string api secret key
	 * @access protected
	 */
	protected $apiSecret;

	/**
	 * Headers to be sent in the api
	 * 
	 * @var array
	 */
	protected $headers = [];

	public function __construct() {

		//sets username and password to be used for api authorization
		$this->username = 'leo.fulgencio@gmail.com';
		$this->password = 'c00kies2000';
		$this->apiUrl = 'https://api.credly.com/v1.1/';

		//Lets set the api key and api secret vars
		$this->apiKey = '929d50c1d5fe37b08b803b41a2ccfbfe';
		$this->apiSecret = 'qayMq3ntsdqAfi+hwwcUmMw54Z4Mzbt8ME+DLh79v7gNttBv+ut8wg4xVbVa0fQSvYNGkkda64U73eP6spWW1vdcwD2Nm52fTxlhq6rGjMTBDako6SkEiyv5w/56Lmp1HH4YDLGJmSVskVEuoB5aIbbOQ5VOhKMHgP6lp3CsRSs=';

		//define headers
		$this->headers = [
			'X-Api-Key' => $this->apiKey,
			'X-Api-Secret' => $this->apiSecret,
			'Authorization' => 'Basic ' . base64_encode( $this->username . ':' . $this->password ) 
		];
	}

    /**
	 * Gets api token to use with api calls for user
	 * 
	 * @param  string 	$api_key	api key that was assigned from api
	 * @param  string 	$api_secret	api secret that was assigned from api
	 * @param  string 	$api_url    url for api calls
	 * @return object 	$apiContent returned json response
	 */
	protected function getAccessToken() {

		//initialize guzzle request to get api token data
		$client = new Guzzle(['base_uri' => $this->apiUrl, 
			'headers' => $this->headers
		]);

		//target post endpoint
		$req = new GuzzleRequest('POST', 'authenticate');

		//send request to get response from client
		$res = $client->send($req);

		//decode json content output from request to PHP object
		$apiContent = json_decode( $res->getBody()->getContents() );

		//Request::session()->put('user_access_token', $apiContent->data->token);

		return $apiContent;

	}

	/**
	 * List members 
	 * 
	 * @param  string 	$email 		email of member
	 * @param  int 		$per_page 	display amount per page
	 * @param  int 		$page 		display page number
	 * 
	 * @return object 	$memberContent content results of returned members
	 */
	protected function listMembers( $email = '', $per_page = null, $page = null ) {

		//get apiToken info
		$accessToken = $this->getAccessToken();

		//GET request query params
		$query = [
			'has_profile' => 1,
			'verbose' => 0,
			'page' => 1,
			'query' => 'user',
			'per_page' => 10,
			'order_direction' => 'ASC',
			'access_token' => $accessToken->data->token
		];

		//lets check if the users email is being passed in
		if( !empty( $email ) )
			$query['email'] = urlencode( $email );

		//lets check if per_page is set
		if( !empty( $per_page ) )
			$query['per_page'] = $per_page;

		//lets check if page is set
		if( !empty( $page ) )
			$query['page'] = $page;

		$memberClient = new Guzzle([
			'base_uri' => $this->apiUrl,
			'headers' => $this->headers,
			'query' => $query
		]);

		$req = new GuzzleRequest('GET', 'members');


		$res = $memberClient->send( $req );

		$memberContent = json_decode( $res->getBody()->getContents() );

		return $memberContent->data;

	}

	/**
	 * Gets single badge information
	 * 
	 * @param  int 		$badgeId
	 * @return array 	array of information for single badge
	 */
	protected function getSingleBadge( $badgeId ) {

		$accessToken = $this->getAccessToken();

		$badgeClient = new Guzzle([
			'base_uri' => $this->apiUrl,
			'headers' => $this->headers,
			'query' => [
				'access_token' => $accessToken->data->token
			]
		]);

		$req = new GuzzleRequest('GET', 'badges/' . $badgeId );

		//lets make sure that the single badge exists
		try {
			$res = $badgeClient->send($req);

			$badgeContent = json_decode( $res->getBody()->getContents() );

			return $badgeContent->data;
			
		} catch( ClientException $e ) {

			//echo $e->getMessage();
			return $e->getResponse()->getStatusCode();

		}

	}

	/**
	 * Gets a list of badges from the API
	 * 
	 * @return object
	 */
	protected function listBadges() {

		$accessToken = $this->getAccessToken();

		$badgeClient = new Guzzle([
			'base_uri' => $this->apiUrl,
			'headers' => $this->headers,
			'query' => [
				'verbose' => 0,
				'page' => 1,
				'per_page' => 10,
				'order_direction' => 'ASC',
				'access_token' => $accessToken->data->token
			]
		]);

		$req = new GuzzleRequest('GET', 'badges' );


		$res = $badgeClient->send($req);

		$badgeContent = json_decode( $res->getBody()->getContents() );

		return $badgeContent->data;

	}

	/**
	 * Gets all badge recipients based on badge ID
	 * 
	 * @param  int 		$badgeId
	 * @return array 	array of recipients containing badge associated wit badge ID
	 */
	protected function getBadgeRecipients( $badgeId ) {

		$accessToken = $this->getAccessToken();

		$badgeRecipientClient = new Guzzle([
			'base_uri' => $this->apiUrl,
			'headers' => $this->headers,
			'query' => [
				'access_token' => $accessToken->data->token
			]
		]);

		$req = new GuzzleRequest('GET', 'badges/' . $badgeId . '/recipients' );

		//check if any exceptions are thrown when checking for recipients of badge
		try {

			$res = $badgeRecipientClient->send($req);

			$badgeRecipientContent = json_decode( $res->getBody()->getContents() );

			return $badgeRecipientContent->data;

		} catch( ClientException $e ) {

			//echo $e->getMessage();
			return $e->getResponse()->getStatusCode();
		}

	}

}
