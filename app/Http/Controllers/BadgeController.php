<?php

namespace App\Http\Controllers;

use Request, Response, View, Session;
use App\Http\Requests;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Exception\RequestException;
use App\CredlyApi;

class BadgeController extends Controller {

	/**
	 * Displays recipients for badge based on badge ID
	 * 
	 * @param  int|string $badgeId
	 * @return Illuminate\View\View
	 */
	public function showBadgeRecipients( $badgeId ) {

		$showBadgeRecipients = CredlyApi::getBadgeRecipients( $badgeId );
		$singleBadge = CredlyApi::getSingleBadge( $badgeId );

		$data = [
			'recipients' => $showBadgeRecipients,
			'badge' => $singleBadge
		];

		return View::make('badge_users', [
			'title' => 'Badge Recipients',
			'data' => $data
		]);

	}

	/**
	 * Displays list of badges
	 * 
	 * @return Illuminate\View\View
	 */
	public function listBadges() {

		$showBadges = CredlyApi::listBadges();

		$badgeArr = [];

		//loops through each object and checks if the badge image url is valid
		foreach( $showBadges as $badge ) {
			
			if( !empty( @getimagesize( $badge->image_url ) ) ) {

				$recipients = CredlyApi::getBadgeRecipients( $badge->id );

				if( $recipients == 404 ) {

					$badgeArr[] = [
                        'recipients_status' => $recipients,
                        'badge' => $badge
                    ]; 

				} else {
                    $badgeArr[]['badge'] = $badge;
                }

			}

		}

		return View::make('badges', [
			'title' => 'Credly Badge List',
			'data' => $badgeArr
		]);

	}

	/**
	 * Queries Badge endpoint from credly API using POST request
	 * 
	 * @return 
	 */
	public function queryBadgeApi() {

		$req = filter_var( Request::input('badge_id'), FILTER_SANITIZE_NUMBER_INT );

		$singleBadge = CredlyApi::getSingleBadge( $req );
		$recipients = CredlyApi::getBadgeRecipients( $req );

		if( !is_object( $recipients ) ) {
			
			Session::flash('badge_error', 'Uh Oh! Badge was not found in the api');
			return redirect('badges');

		} else {

			return View::make('badge_users', [
				'title' => 'Badge Recipients',
				'data' => [
					'badge' => $singleBadge,
					'recipients' => $recipients
				]
			]);

		}

	}

}
