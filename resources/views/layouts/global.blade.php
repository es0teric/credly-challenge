<!DOCTYPE html>

<html lang="en">

	<head>
		<title>{{ $title }}</title>
		
		<link rel="stylesheet" href="/assets/framework/bootstrap/css/bootstrap.min.css" type="text/css" media="screen" />
		<script type="text/javscript" src="/assets/framework/bootstrap/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="/assets/css/app.css" type="text/css" media="screen" />

	</head>

	<body>

		@if( Session::has('badge_error') )
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ Session::get('badge_error') }}
			</div>
		@endif

		<div class="container">
			@yield('content')
		</div>
	</body>

</html>