@extends('layouts.global')

@section('content')

	<div class="row">

		<div id="user-form" class="col-sm-5 col-centered">
			
			{{ Form::open([ 'action' => 'BadgeController@queryBadgeApi', 'method' => 'POST', 'class' => 'form-horizontal' ]) }}
				
				<div class="form-group">

					<label class="col-sm-3 control-label">Badge ID</label>
					<div class="col-sm-9">
					  <input type="text" class="form-control" name="badge_id" placeholder="Badge ID" />
					</div>

				</div> <!-- /.form-group -->
				
				<div class="form-group">
				    <div class="col-sm-offset-3 col-sm-10">
				      <button type="submit" class="btn btn-default">Search</button>
				    </div>
				</div> <!-- /.form-group -->

			{{ Form::close() }}

		</div> <!-- /#user-form -->

	</div> <!-- /.row -->

	<div class="row">

		<div class='col-sm-10 col-centered'>
			<ul class="list-inline">
				@foreach( $data as $badge )
					<li>
						@if( !isset( $badge['recipients_status'] ) )
						<a href="/badge/{{ $badge['badge']->id }}">
							<img src="{{ $badge['badge']->image_url }}" width="250" height="250" />
						</a>
						@else
							<img src="{{ $badge['badge']->image_url }}" width="250" height="250" />
						@endif
					</li>
				@endforeach
			</ul>
		</div> <!-- /.col-sm-5 -->

	</div> <!--/.row -->
@stop