@extends('layouts.global')

@section('content')

	<div class="row">
		<div class="col-sm-3 col-centered">
			<img src="{{ $data['badge']->image_url }}" width="250" height="250" />
		</div> <!-- /.col-sm-3 -->
	</div> <!-- /.row -->

	<div class="row">

		<div class='col-sm-10 col-centered'>

			@if( $data['recipients'] == 404 )
				
				<h3>This badge has no recipients.</h3>

			@else

			<ul class="list-inline">
				@foreach( $data['recipients'] as $recipient )
					<li>
						<img src="{{ $recipient->avatar }}" width="150" height="150" />
						<p>{{ $recipient->display_name }}</p>
					</li>
				@endforeach
			</ul>
			@endif
		</div> <!-- /.col-sm-5 -->

	</div> <!--/.row -->
@stop